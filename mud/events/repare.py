# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class RepareEvent(Event2):
    NAME="repare"

    def perform(self):
        listeid=["roue-000","roueNeuve-000","cléAnglaise-000","chasse-neige-000","huile-000"]
        cpt=0
        for x in  self.actor.contents():
            if x.id in listeid:
                cpt+=1
        if cpt==len(listeid):
            self.inform("repare")
        elif not self.object.has_prop("reparable") or cpt!=len(listeid):
            self.fail()
