# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import RepareEvent

class RepareAction(Action2):
    EVENT = RepareEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "repare"
